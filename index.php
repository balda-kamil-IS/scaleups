<?php /* EXDITO PROLOG BEGIN */require_once(dirname(__FILE__) . "/./../../system/frontman/header.php");/* EXDITO PROLOG END */ ?>

<?php $APP->setPageLayout('notitle'); ?>

  <!-- #### SECTION SLIDER #### -->

  <section class="section-slider ">
    <div class="item" style="min-height: 400px; background-image:url('<?php echo BASE_URL ?>/infoshare/_media/video/infoshare_baner2_czerwony.jpg');">
      <div class="video-container">
        <video muted autoplay="" class="video-bg" loop="loop" preload="auto">
          <source src="<?php echo BASE_URL ?>/infoshare/_media/video/infoshare_baner2_czerwony.webm" type="video/webm"/>
          <source src="<?php echo BASE_URL ?>/infoshare/_media/video/infoshare_baner2_czerwony.mp4" type="video/mp4"/>
        </video>
      </div>
      <div class="item-txt">
        <img src="<?php echo BASE_URL ?>/infoshare/startup-contest-2018/logo/isc_logo_white.svg" style="max-height:260px; max-width:220px; margin-bottom: 20px;"/>
        <p class="fs-20 fs-lg-30 mb-20 color-white">Are you a social impact scaleup?</p>
        <p class="fs-20 fs-lg-30 mb-20 color-white">Are you ready to present at infoShare Growth Stage?</p>
        <p class="fs-20 fs-lg-30 mb-20 color-white">Are you ready to get attention of 6k+ crowd, including investors? </p>
        <div class="t-center wow fadeInUp" data-wow-delay="0.6s" data-wow-offset="-200">
        <a class="fs-lg-42 btn big btn-empty-red tt-none fw-900" href="https://infoshare.pl/is-register/?add=117">APPLY</a>
        </div>
      </div>
    </div>
  </section>

  <!-- #### SECTION GANDI QUOTE #### -->
  <section class="html-editor bx01 section-padding t-center">
      <h2 class="fs-sm-40 fs-xs-26 is-h3-quote">“Be the change that you wish to see in the world.”</h2> <cite>Gandhi</cite>
      <p class="mt-40 pl-20 pr-20 fs-sm-26 fs-xs-22 is-p-center">infoShare will choose up to <strong>10 scaleups</strong> with <strong>social impact</strong> which will get <strong>FOR FREE a 5</strong>-minute slot at Growth Stage to present the impact they want to achieve. </p>
  </section>

  <!--CONTEST LIST-->
  <section class="container-fluid bg-img02 color-white nop nom pb-60" style="z-index: 69;">
    <div class="clearfix">
      <div class=" col-xs-12 nop flex-items center">
        <div class="t-center" style="max-width: 1200px; margin: 0 auto;">
          <h1 class="pt-60 fw-900">What is social impact?</h1>
          <h3>Sustainable Development Goals by United Nations:</h3>
            <div class="flex-container t-center">
                <div class="col-lg-2 col-md-3 col-xs-6 m-10 flex-items wow fadeInLeft is-white-border" style="align-items: center">
                    <p class="m-0 tt-uppercase">1. no poverty</i></p>
                </div>

                <div class="col-lg-2 col-md-3 col-xs-6 m-10 flex-items wow fadeInLeft is-white-border" style="align-items: center">
                    <p class="m-0 tt-uppercase">2. zero hunger</i></p>
                </div>

                <div class="col-lg-2 col-md-3 col-xs-6 m-10 flex-items wow fadeInLeft is-white-border" style="align-items: center">
                    <p class="m-0 tt-uppercase">3. good health and well-being</i></p>
                </div>

                <div class="col-lg-2 col-md-3 col-xs-6 m-10 flex-items wow fadeInLeft is-white-border" style="align-items: center">
                    <p class="m-0 tt-uppercase">4. quality education</i></p>
                </div>

                <div class="col-lg-2 col-md-3 col-xs-6 m-10 flex-items wow fadeInLeft is-white-border" style="align-items: center">
                    <p class="m-0 tt-uppercase">5. gender equality</i></p>
                </div>

                <div class="col-lg-2 col-md-3 col-xs-6 m-10 flex-items wow fadeInLeft is-white-border" style="align-items: center">
                    <p class="m-0 tt-uppercase">6. clean water and sanitation</i></p>
                </div>

                <div class="col-lg-2 col-md-3 col-xs-6 m-10 flex-items wow fadeInLeft is-white-border" style="align-items: center">
                    <p class="m-0 tt-uppercase">7. affordable and clean energy</i></p>
                </div>

                <div class="col-lg-2 col-md-3 col-xs-6 m-10 flex-items wow fadeInLeft is-white-border" style="align-items: center">
                    <p class="m-0 tt-uppercase">8. decent work and economic growth</i></p>
                </div>

                <div class="col-lg-2 col-md-3 col-xs-6 m-10 flex-items wow fadeInLeft is-white-border" style="align-items: center">
                    <p class="m-0 tt-uppercase">9. industry, innovation and infrastructure</i></p>
                </div>

                <div class="col-lg-2 col-md-3 col-xs-6 m-10 flex-items wow fadeInLeft is-white-border" style="align-items: center">
                    <p class="m-0 tt-uppercase">10. reduced inequalities</i></p>
                </div>

                <div class="col-lg-2 col-md-3 col-xs-6 m-10 flex-items wow fadeInLeft is-white-border" style="align-items: center">
                    <p class="m-0 tt-uppercase">11. sustainable cities and communities</i></p>
                </div>

                <div class="col-lg-2 col-md-3 col-xs-6 m-10 flex-items wow fadeInLeft is-white-border" style="align-items: center">
                    <p class="m-0 tt-uppercase">12. responsible consumption and production</i></p>
                </div>

                <div class="col-lg-2 col-md-3 col-xs-6 m-10 flex-items wow fadeInLeft is-white-border" style="align-items: center">
                    <p class="m-0 tt-uppercase">13. climate action</i></p>
                </div>

                <div class="col-lg-2 col-md-3 col-xs-6 m-10 flex-items wow fadeInLeft is-white-border" style="align-items: center">
                    <p class="m-0 tt-uppercase">14. life below water</i></p>
                </div>

                <div class="col-lg-2 col-md-3 col-xs-6 m-10 flex-items wow fadeInLeft is-white-border" style="align-items: center">
                    <p class="m-0 tt-uppercase">15. life on land</i></p>
                </div>

                <div class="col-lg-2 col-md-3 col-xs-6 m-10 flex-items wow fadeInLeft is-white-border" style="align-items: center">
                    <p class="m-0 tt-uppercase">16. peace, justice and strong institutions</i></p>
                </div>

                <div class="col-lg-2 col-md-3 col-xs-6 m-10 flex-items wow fadeInLeft is-white-border" style="align-items: center">
                    <p class="m-0 tt-uppercase">17. partnership for the goals</i></p>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


<!-- #### SECTION SHEDULE ##### -->

    <section class="bx01">
        <section style="max-width: 1000px; margin: 0 auto;">
        <h1 class="t-center fs-22 fs-lg-42 fw-900 pt-60">SCHEDULE - 4 steps</h1>
        <h3 class="t-center">How to win your very own 5-minute time at the Growth Stage?</h3>

            <?php
            $inactive = "style='opacity:0.5 !important;'";
            ?>

    <div class="flex-container t-center pb-30">
            <div class="col-lg-3 col-md-6 col-xs-6 full-xxs flex-items items-top wow fadeInLeft">
                <div class="bx-rel" style="">
                    <div class="num-step" style=""><span>1</span></div>
                    <p class="nom fw-600" style="width: 100%; bottom: 30px;">no later than
                        28.02.2019</p>
                    <?php if (new DateTime() > new DateTime("2019-02-28 23:59:59")): ?>
                        <p class="nom fw-600" style="; width: 100%; bottom: 0;">FINISHED</p>
                    <?php endif; ?><br/>

                    <p class="sm fw-600">Social Impact Scaleups application process</p>
                    <p class="sm fw-300">Buy Startup Pass and check the right box. Send us your detailed application with presentation attached. We are going to choose up to 10 most interesting projects. Use your chance wisely.</p>
                </div>
            </div><!-- .col-2 -->

            <div class="col-lg-3 col-md-6 col-xs-6 full-xxs flex-items items-top wow fadeInLeft" data-wow-delay="0.1s">
                <div class="bx-rel" style="">
                    <div class="num-step"><span>2</span></div>
                    <p class="nom fw-600" style="width: 100%; bottom: 30px;">no later than 08.03.2019</p>
                    <?php if (new DateTime() > new DateTime("2019-03-08 23:59:59")): ?>
                        <p class="nom fw-600" style="width: 100%; bottom: 0;">FINISHED</p>
                    <?php endif; ?><br/>
                    <p class="sm fw-600">up to 10 Social Impact Scaleups announcement</p>
                    <p class="sm fw-300">Check our website to see which 10 Social Impact Scaleups are going to present at Growth Stage at infoShare 2019.</p>
                </div>
            </div><!-- .col-2 -->

            <div class="col-lg-3 col-md-6 col-xs-6 full-xxs flex-items items-top wow fadeInLeft" data-wow-delay="0.1s">
                <div class="bx-rel" style="">
                    <div class="num-step"><span>3</span></div>
                    <p class="nom fw-600" style="width: 100%; bottom: 30px;">07.05.2019</p>
                    <?php if (new DateTime() > new DateTime("2019-05-07 23:59:59")): ?>
                        <p class="nom fw-600" style="width: 100%; bottom: 0;">FINISHED</p>
                    <?php endif; ?><br/>
                    <p class="sm fw-600">Social Impact Scaleups presentation coaching</p>
                    <p class="sm fw-300">Reserve time for a workshop letting you put the finishing touches on your presentation. Use all tips & tricks to charm the audience.</p>
                </div>
            </div><!-- .col-2 -->

            <div class="col-lg-3 col-md-6 col-xs-6 full-xxs flex-items items-top wow fadeInLeft" data-wow-delay="0.2s">
                <div class="bx-rel" style="">
                    <div class="num-step"><span>4</span></div>
                    <p class="nom fw-600" style=" width: 100%; bottom: 30px;">09.05.2019</p>
                    <?php if (new DateTime() > new DateTime("2019-05-09 23:59:59")): ?>
                        <p class="nom fw-600" style=" width: 100%; bottom: 0;">FINISHED</p>
                    <?php endif; ?><br/>
                    <p class="sm fw-600">Social Impact Scaleups presentations at the Growth Stage</p>
                    <p class="sm fw-300">Get ready to present in front of 6k+ crowd, including investors and experts. Make the most of your 5 minutes on Stage to win their attention. </p>
                </div>
            </div><!-- .col-2 -->
        </div><!-- .flex-container -->
        </section>
    </section>

  <!-- #### SECTION CONTACT #### -->
  <section class="bg-img02 color-white nop nom pt-60 pb-60" style="z-index: 69;">
    <div class="container">
      <h1 class="t-center fs-22 fs-lg-42 mb-30 fw-900">CONTACT</h1>
      <div class="row">
        <div class="col-md-6 col-xs-12">
          <h3 class="t-center mb-10">Questions</h3>
          <div class="user-profil p-20" style="background: transparent">
            <div class="user-profil-img">
              <img src="<?php echo BASE_URL ?>/infoshare/_media/team_person/team_contact/infoshare_team_kacper.JPG" style="border-color: transparent;"/>
            </div>
            <div class="user-profil-desc wrap">
              <h5 class="nom"><strong>Kacper Bałajewicz</strong></h5>
              <p class="mb-10">Startup Community Support</p>
              <p class="mb-10"><a href="mailto:startup@infoshare.pl">startup@infoshare.pl</a></p>
              <p class="nom color-white"><a href="tel:+48 504 405 123">+48 509 013 783</a></p>
              <p></p>
            </div>
            <div class="clear"> </div>
          </div>
        </div>

        <div class="col-md-6 col-xs-12">
          <h3 class="t-center mb-10">Organizer</h3>
          <div class="user-profil p-20" style="background: transparent">
            <div class="user-profil-img">
              <img src="<?php echo BASE_URL ?>/infoshare/_media/logos/infoshare_logo_short_RGB_RED_TRANSPARENT.png" style="border-color: transparent;"/>
            </div>
            <div class="user-profil-desc wrap">
              <h5 class="nom"><strong>Fundacja infoShare</strong></h5>
              <p class="nom">
                Olivia Four<br/>
                al. Grunwaldzka 472b<br/>
                Olivia Four, 2nd floor<br/>
                80-309 Gdańsk
              </p>
              <p class="t-left nom">Our office is located in  <a href="http://www.oliviacentre.com/rooms/connect/">Olivia Business Centre!</a></p>
            </div>
            <div class="clear"> </div>
          </div>
        </div>

      </div><!-- .row -->
    </div><!-- .container -->
  </section>

  <link rel="stylesheet" href="is-style.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

<?php /* EXDITO EPILOG BEGIN */require_once(dirname(__FILE__) . "/./../../system/frontman/footer.php");/* EXDITO EPILOG END */ ?>